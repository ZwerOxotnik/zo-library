# Библиотека ZwerOxotnik'а

Хотите прочитать на другом языке? | [English](/README.md)
|---|---|

## Быстрые ссылки

[Список изменений](/CHANGELOG.md)
|---|

## Содержание

* [Введение](#overview)
* [Сообщить об ошибки](#issue)
* [Запросить функцию](#feature)
* [Установка](#installing)
* [Лицензия](#license)

## <a name="overview"></a> Введение

Универсальная библиотека для статичного + динамического использования.\
Комманды игрокам, функиям разработчика модов.\
Сообщения о событиях PvP в Discord'е

Добавлено:\
Поддержка Factocord 3.0 (Factorio + Discord)\
Получение реального ника игрока (0.17)\
Сообщения о PvP событиях в Discord

## <a name="issue"></a> Нашли ошибку?

Пожалуйста, сообщайте о любых проблемах или ошибках в документации, вы можете помочь нам
[submitting an issue](https://gitlab.com/ZwerOxotnik/zo-library/issues) на нашем GitLab репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/zo-library/discussion).

## <a name="feature"></a> Хотите новую функцию?

Вы можете *запросить* новую функцию [submitting an issue](https://gitlab.com/ZwerOxotnik/zo-library/issues) на нашем GitLab репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/zo-library/discussion).

## <a name="installing"></a> Установка

Если вы скачали zip архив:

* просто поместите его в директорию модов.

Для большей информации, смотрите [вики Factorio "загрузка и установка модов"](https://wiki.factorio.com/Modding/ru#.D0.97.D0.B0.D0.B3.D1.80.D1.83.D0.B7.D0.BA.D0.B0_.D0.B8_.D1.83.D1.81.D1.82.D0.B0.D0.BD.D0.BE.D0.B2.D0.BA.D0.B0_.D0.BC.D0.BE.D0.B4.D0.BE.D0.B2).

Если вы скачали исходный архив (GitLab):

* скопируйте данный мод в директорию модов Factorio
* переименуйте данный мод в secondary-chat_*версия*, где *версия* это версия мода, которую вы скачали (например, 0.2.0)

## <a name="license"></a> Лицензия

Этот проект защищен авторским правом © 2019-2020 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Использование исходного кода, включенного здесь, регламентируется European Union Public License v. 1.2 только. Смотрите [LICENCE](/LICENCE) файл для разбора.

[homepage]: http://mods.factorio.com/mod/zo-library
[Factorio]: https://factorio.com/
