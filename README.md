# ZwerOxotnik's library

Read this in another language | [English](/README.md)
|---|---|

## Quick Links

[Changelog](CHANGELOG.md)
|---|

## Contents

* [Overview](#overview)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [License](#license)

## Overview

Universal library for static + dynamic using.\
Commands to players, functions to mod developers.

Added:\
Support of Factocord 3.0 (Factorio + Discord)\
Getting real nickname of a player (0.17)\
Messages about PvP events in Discord

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by
[submitting an issue](https://gitlab.com/ZwerOxotnik/zo-library/issues) to our GitLab Repository or on [mods.factorio.com](https://mods.factorio.com/mod/zo-library/discussion).

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue](https://gitlab.com/ZwerOxotnik/zo-library/issues) to our GitHub
Repository or on [mods.factorio.com](https://mods.factorio.com/mod/zo-library/discussion).

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to secondary-chat_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 0.2.0)

## License

This project is copyright © 2019-2020 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Use of the source code included here is governed by the European Union Public License v. 1.2 only. See the [LICENCE](/LICENCE) file for details.

[homepage]: http://mods.factorio.com/mod/zo-library
[Factorio]: https://factorio.com/
