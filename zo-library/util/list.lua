-- Copyright (C) 2019-2020 ZwerOxotnik <zweroxotnik@gmail.com>
-- Licensed under the EUPL, Version 1.2 only (the "LICENCE");

-- You can attach this library via \/
-- local zo_library = require("__zo_library__/zo_library/util/list")

local zo_util = {}

zo_util.factocord = require("__zo-library__/zo-library/util/zo_factocord")

return zo_util